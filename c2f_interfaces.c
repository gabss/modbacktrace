#include <stdlib.h>

//gfortran, ifort and xlf compatiable c2fortran bindings

/* Fortran binding for 'libdbg_invoke_adress_to_line_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_invoke_adress_to_line_f_(int * adr_cnt,unsigned long int *adr)
{
    dbg_invoke_adress_to_line(adr_cnt,adr);
}

/* Fortran binding for 'libdbg_activate_fpu_traps_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_activate_fpu_traps_f_()
{
    dbg_activate_fpu_traps();
}


/* Fortran binding for 'libdbg_deactivate_fpu_traps_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_deactivate_fpu_traps_f_()
{
    dbg_deactivate_fpu_traps();
}

/* Fortran binding for 'libdbg_emit_back_trace_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_emit_back_trace_f_(int* id)
{
    dbg_emit_back_trace(*id);
}

/* Fortran binding for 'libdbg_init_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_init_f_(int* sig_level)
{
    dbg_init(*sig_level);
}

/* Fortran binding for 'libdbg_get_back_trace_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libdbg_get_back_trace_f_(int* adr_cnt,long int * adresses)
{
    dbg_get_back_trace(adr_cnt,adresses);
}


/*To Fortran Interface:

    libstat_ShowMemStats()
    libstat_Start()
    libstat_Stop()
    libstat_GetMemStats(int64_t *pmem_allocated,
                         int64_t *pmem_alloc_peak,
                         int64_t *pmem_alloc_cnt,
                         int64_t *pmem_free_cnt,
                         int64_t *pmem_realloc_cnt,
                         int64_t *pmem_memalign_cnt)
    libstat_ResetMemStats()

*/
/*
    these wrappers will be modified by f2cintf.py
*/

/* Fortran binding for 'libstat_ShowMemStats_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_showmemstats_f_()
{
    libstat_ShowMemStats();
}


/* Fortran binding for 'libstat_Start_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_start_f_()
{
    libstat_Start();
}


/* Fortran binding for 'libstat_Stop_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_stop_f_()
{
    libstat_Stop();
}


/* Fortran binding for 'libstat_ResetMemStats_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_resetmemstats_f_()
{
    libstat_ResetMemStats();
}


/* Fortran binding for 'libstat_GetMemStats_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_getmemstats_f_ (int64_t *pmem_allocated,
                         int64_t *pmem_alloc_peak,
                         int64_t *pmem_alloc_peak2,
                         int64_t *pmem_alloc_cnt,
                         int64_t *pmem_free_cnt,
                         int64_t *pmem_realloc_cnt,
                         int64_t *pmem_memalign_cnt)
{
    libstat_GetMemStats(pmem_allocated,
                         pmem_alloc_peak,
                         pmem_alloc_peak2,
                         pmem_alloc_cnt,
                         pmem_free_cnt,
                         pmem_realloc_cnt,
                         pmem_memalign_cnt);
}


/* Fortran binding for 'libstat_ResetPeak_f' computed for compiler 'mpiifort' by f2cintf.py*/
void libstat_resetpeak_f_()
{
    libstat_ResetPeak();
}


