  SUBROUTINE backtrace_setup(sig_level)
    INTEGER,INTENT(IN)::sig_level

    CALL libdbg_init_f(sig_level)

  END SUBROUTINE

  SUBROUTINE backtrace_emit(tagid)
    INTEGER,INTENT(IN)::tagid

    CALL libdbg_emit_back_trace_f(tagid)

  END SUBROUTINE


  SUBROUTINE backtrace_print(iunit)
    INTEGER,INTENT(IN)::iunit

    INTEGER*8::back_trace(128)
    INTEGER i,back_cnt

    WRITE(iunit,*) 'Printing back trace:'

    back_cnt=128

    CALL libdbg_get_back_trace_f(back_cnt,back_trace(1))


!this is done because for now 'libdbg_invoke_adress_to_line_f' can only
!print to stdout, hence iunit=6

    IF(iunit==6) THEN
#ifdef NO_ADDRTOLINE
DO i=1,back_cnt
        WRITE(iunit,'(I4,A)',advance='no') i,' -> '

        CALL print_hex(iunit,back_trace(i))
      END DO
#else

#ifndef NOFLUSH
CALL flush(iunit)
#endif


      CALL libdbg_invoke_adress_to_line_f(back_cnt,back_trace(1))
#endif
ELSE
      DO i=1,back_cnt
        WRITE(iunit,'(I4,A)',advance='no') i,' -> '

        CALL print_hex(iunit,back_trace(i))
      END DO
    END IF

#ifndef NOFLUSH
CALL flush(iunit)
#endif
END SUBROUTINE


  SUBROUTINE print_hex(iunit,v)
    INTEGER,INTENT(IN)::iunit
    INTEGER*8,INTENT(IN)::v
    INTEGER*8 x
    INTEGER*8 rem
    CHARACTER (LEN=16) hex_digits

    CHARACTER (LEN=16) hex_number
    INTEGER i

    DO i=1,16
      hex_number(i:i)='0'
    END DO

    IF(v<0) WRITE(iunit,'(A)',advance='no') '-'

    hex_digits='0123456789abcdef'

    x=abs(v)

    i=16

    DO WHILE(x>0)

      rem= MODULO(x,16) +1

      hex_number(i:i)=hex_digits(rem:rem)

      i=i-1
      x=x/16

    END DO

    WRITE(iunit,'(A)') hex_number
  END SUBROUTINE

  SUBROUTINE enable_fpu_traps

    CALL libdbg_activate_fpu_traps_f

  END SUBROUTINE


  SUBROUTINE disable_fpu_traps

    CALL libdbg_deactivate_fpu_traps_f

  END SUBROUTINE
