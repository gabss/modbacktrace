
#include <memory.h>
#include <unistd.h>
#include <signal.h>
#include <ucontext.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <sys/resource.h>
#include <string.h>
#include <stdio.h>
#include <mpi.h>


static struct {int signal;char*descr;}s_sig_descr[]={
    {SIGABRT,	"Process aborted"},
    {SIGALRM,	"Signal raised by alarm"},
    {SIGBUS,	"Bus error: access to undefined portion of memory object"},	
    {SIGCHLD,	"Child process terminated, stopped (or continued*)"},	
    {SIGCONT,	"Continue if stopped"},
    {SIGFPE,	"Floating point exception: erroneous arithmetic operation"},	
    {SIGHUP,	"Hangup"	},
    {SIGILL,	"Illegal instruction"	},
    {SIGINT,	"Interrupt"	},
    {SIGKILL,	"Kill (terminate immediately)"	},
    {SIGPIPE,	"Write to pipe with no one reading"	},
    {SIGQUIT,	"Quit and dump core"	},
    {SIGSEGV,	"Segmentation violation"	},
    {SIGSTOP,	"Stop executing temporarily"	},
    {SIGTERM,	"Termination (request to terminate)"	},
    {SIGTSTP,	"Terminal stop signal"	},
    {SIGTTIN,	"Background process attempting to read from tty (in)"	},
    {SIGTTOU,	"Background process attempting to write to tty (out)"	},
    {SIGUSR1,	"User-defined 1"	},
    {SIGUSR2,	"User-defined 2"	},
    {SIGPOLL,	"Pollable event"	},
    {SIGPROF,	"Profiling timer expired"},	
    {SIGSYS,	"Bad syscall"	},
    {SIGTRAP,	"Trace/breakpoint trap"	},
    {SIGURG,	"Urgent data available on socket"	},
    {SIGVTALRM,	"Signal raised by timer counting virtual time: virtual timer expired"	},
    {SIGXCPU,	"CPU time limit exceeded"	},
    {SIGXFSZ,	"File size limit exceeded"	},
    {0,	NULL	}
};




static int dbg_my_rank;
static int dbg_my_pid;




char * get_sig_descr(int signum)
{
    int i;
    for(i=0;s_sig_descr[i].descr!=NULL;i++)
    {
        if(s_sig_descr[i].signal=signum)
        {
            return s_sig_descr[i].descr;
        }
    }
    
    return "(NO DESCRIPTION)";
}


size_t getRSS()
{
        struct rusage ru;
	getrusage(RUSAGE_SELF,&ru);

	return ru.ru_maxrss;
}

static void signal_toabort(int signum) 
{

  
    
    struct timeval tv;
    struct timezone tz;
    void *array[128];
    size_t size;
    size_t i;
	
    gettimeofday(&tv,&tz);
    
    char buf[256];
    
    
    sprintf(buf,"rank_%05i_sig_dbg.backtrace",dbg_my_rank);
    
    FILE*f=fopen(buf,"wt");
    
    if(f==NULL)
    {
        f=stderr;
    }
    

    fprintf(f,"#----------------SIGNAL-INFO-START------------------------\n");
    fprintf(f,"SIGNAL ID = %02d \n",signum);
    fprintf(f,"SIGNAL DESC = %s\n",get_sig_descr(signum));
    fprintf(f,"MPI GLOBAL RANK %06d\n",dbg_my_rank);
    fprintf(f,"PROCESS ID %06d\n",dbg_my_pid);
    fprintf(f,"TIME %09d.%06d\n\n",tv.tv_sec,tv.tv_usec);

//get resident size
    size_t rss=getRSS();
    
    fprintf(f,"MEMORY RSS = %d\n",rss);

    fprintf(f,"\n#BACK TRACE:\n"); 

    size = backtrace (array, 128);
// strings = backtrace_symbols (array, size);

    fprintf (f,"#BackTrace %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
    {
      fprintf (f,"%lx\n", array[i]);
    }

    


    
    

    fprintf(f,"#----------------SIGNAL-INFO-END-------------------------\n");
 
    fflush(f); 
    if(f!=stderr)  fclose(f);
    //abort();  

    signal(signum,SIG_DFL);
    raise(signum);
}


static void capture_signal(int num)
{
    signal(num,signal_toabort);

}

void at_exit_hanlder()
{
    struct timeval tv;
    struct timezone tz;
    void *array[128];
    size_t size;
    size_t i;
	
    gettimeofday(&tv,&tz);
    
    char buf[256];
    
    
    sprintf(buf,"rank_%05i_atexit_dbg.backtrace",dbg_my_rank);
    
    FILE*f=fopen(buf,"wt");
    
    if(f==NULL)
    {
        f=stderr;
    }
    

    fprintf(f,"#----------------AT    EXIT   HANDLER------------------------\n");
    fprintf(f,"MPI GLOBAL RANK = %06d\n",dbg_my_rank);
    fprintf(f,"PROCESS ID = %06d\n",dbg_my_pid);
    fprintf(f,"TIME = %09d.%06d\n\n",tv.tv_sec,tv.tv_usec);
    fprintf(f,"#Note:This is an at exit handler, it is called even if program has normal exit\n\n");

//get resident size
    size_t rss=getRSS();

    
    fprintf(f,"MEMORY RSS = %d\n",rss);
    

    fprintf(f,"\n#BACK TRACE:\n"); 

    size = backtrace (array, 128);
// strings = backtrace_symbols (array, size);

    fprintf (f,"#BackTrace %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
    {
      fprintf (f,"%lx\n", array[i]);
    }

    

    fprintf(f,"#----------------SIGNAL-INFO-END-------------------------\n");
 
    fflush(f); 
    if(f!=stderr)  fclose(f);
    
}


//list of signals to capture
static int sig_no_sigs[]={0};
static int sig_to_segv[]={11,0};
static int sig_to_segv_abort[]={9,10,11,15,0};

static int sig_to_all_bad[]={4,7,6,9,10,11,15,0};


void dbg_init(int signal_level)
{
    MPI_Comm_rank(MPI_COMM_WORLD,&dbg_my_rank);
    dbg_my_pid=getpid();
    
    
    /*if(dbg_my_rank==0) 
    {
        fprintf(stderr,"lib_dbg_init\n");
        fflush(stderr);
    }*/
    
    int i;
    
    int* sig_to_capture=NULL;
    
    switch(signal_level)
    {
        case -1:
            sig_to_capture=sig_no_sigs;
            if(dbg_my_rank==0) 
            {
                fprintf(stderr,"lib_dbg not capturing anything\n");
                fflush(stderr);
            }
            break;
        case 0:
            sig_to_capture=sig_to_segv;
            if(dbg_my_rank==0) 
            {
                fprintf(stderr,"lib_dbg capturing only sigsegv\n");
                fflush(stderr);
            }
            break;
        case 1:
            sig_to_capture=sig_to_segv_abort;
            if(dbg_my_rank==0) 
            {
                fprintf(stderr,"lib_dbg capturing only sigsegv and sigabort\n");
                fflush(stderr);
            }
            break;
        case 2:
            sig_to_capture=sig_to_all_bad;
            
            atexit(at_exit_hanlder);
            
            if(dbg_my_rank==0) 
            {
                fprintf(stderr,"lib_dbg capturing all bad signals and installing at_exit handler\n");
                fflush(stderr);
            }
            
            break;
        default:
            //otherwise capture everything
            sig_to_capture=sig_to_all_bad;
    }
    
    
    for(i=0;sig_to_capture[i];i++)
    {
      if(dbg_my_rank==0) 
      {
          fprintf(stderr,"lib_dbg signals capturing signal %i - '%s'\n",sig_to_capture[i],get_sig_descr(sig_to_capture[i]));
      }
      
      capture_signal(sig_to_capture[i]);
      
    }
    
    if(dbg_my_rank==0)
    {
          fflush(stderr);
    }
}  


void print_backtrace(FILE*f)
{
     void *array[128];
    size_t size;
    size_t i;
    size = backtrace (array, 128);
// strings = backtrace_symbols (array, size);

    fprintf (f,"#BackTrace %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
    {
      fprintf (f,"%lx\n", array[i]);
    }
}

void dbg_emit_back_trace(int identifier)
{
    struct timeval tv;
    struct timezone tz;
    void *array[128];
    size_t size;
    size_t i;
	
    gettimeofday(&tv,&tz);
    
    char buf[256];
    
    
    sprintf(buf,"rank_%05i_user.backtrace",dbg_my_rank);
    
    FILE*f=fopen(buf,"a");
    
    if(f==NULL)
    {
        f=stderr;
    }
    

    fprintf(f,"#----------------USER BACTRACE------------------------\n");
    fprintf(f,"IDENTIFIER = %08d\n",identifier);
    fprintf(f,"MPI GLOBAL RANK = %06d\n",dbg_my_rank);
    fprintf(f,"PROCESS ID = %06d\n",dbg_my_pid);
    fprintf(f,"TIME = %09d.%06d\n\n",tv.tv_sec,tv.tv_usec);
//get resident size
    size_t rss=getRSS();

    
    fprintf(f,"MEMORY RSS = %d\n",rss);
    

    fprintf(f,"#BACK TRACE:\n"); 

    size = backtrace (array, 128);
// strings = backtrace_symbols (array, size);

    fprintf (f,"#BackTrace %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
    {
      fprintf (f,"%lx\n", array[i]);
    }

    print_backtrace(f);


    fprintf(f,"#----------------USER BACTRACE-------------------------\n");
 
    fflush(f); 
    if(f!=stderr)  fclose(f);
    
}

//This routine is linux specific

void get_self_path_name(char* path,int max_path_size)
{
    
    int err=readlink("/proc/self/exe",path,max_path_size);
    
    if(err<0)
    {
        strncpy(path,"/proc/self/exe",max_path_size);
    }
}

int adress_to_line(unsigned long int adr)
{
    char cmd_line[2048];   
    char buffer[1024];
   
    memset(buffer,0,sizeof(buffer));
 
    get_self_path_name(buffer,sizeof(buffer));
    
    sprintf(cmd_line,"addr2line -e %s %lx",buffer,adr);
    
    fprintf(stdout,"%0.16x: ",adr);
    
    fflush(stdout);
    
    int ret=system(cmd_line);
    
    fflush(stdout);
    
    return ret;
}

void dbg_invoke_adress_to_line(int * adr_cnt,unsigned long int *adr)
{
    int i;
    for(i=0;i<*adr_cnt;i++)
    {
       adress_to_line(adr[i]);   
    }
}

void dbg_get_back_trace(int* adr_cnt,long int * adresses)
{
    void *array[128];
    size_t size;
    size_t i;
    
    
    size = backtrace (array, 128);
    
    size=(*adr_cnt)<size?(*adr_cnt):size;
    
    for (i = 0; i < size; i++)
    {
        adresses[i]=(long int)array[i];
    }
    
    *adr_cnt=size;
}



#define _GNU_SOURCE
#include <fenv.h>

void dbg_activate_fpu_traps()
{
    feenableexcept(FE_INVALID   | 
    FE_DIVBYZERO | 
    FE_OVERFLOW  | 
    FE_UNDERFLOW);
}

void dbg_deactivate_fpu_traps()
{
    fedisableexcept(FE_INVALID   | 
    FE_DIVBYZERO | 
    FE_OVERFLOW  | 
    FE_UNDERFLOW);
}





