/*
  Written by S.Jaure 
  derived from coupler code, allows to compute malloc statistics by hooking malloc and related functions.
  Fortran interface added at the end(_f) added at the end. Use f2cintf.py to generate the bindings
*/

/* Prototypes for __malloc_hook, __free_hook */
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>


/* Prototypes for our hooks.  */
static void mp_init_hook ( void );
static void *mp_malloc_hook ( size_t, const void * );
static void mp_free_hook ( void*, const void * );
static void *mp_realloc_hook(void*, size_t, const void * );
static void *mp_memalign_hook(size_t ,size_t,const void *);



static size_t mem_allocated=0;
static size_t mem_alloc_peak=0;
static size_t mem_alloc_peak2=0;
static size_t mem_alloc_cnt=0;
static size_t mem_realloc_cnt=0;
static size_t mem_memalign_cnt=0;
static size_t mem_free_cnt=0;






#define BYTE        (1L)
#define KILO_BYTE   (1024L*BYTE)
#define MEGA_BYTE   (1024L*KILO_BYTE)
#define GIGA_BYTE   (1024L*MEGA_BYTE)
#define TERA_BYTE   (1024L*GIGA_BYTE)

char* format_human_readable_size(char* buf,size_t size)
{
    
    double floating_size;
    char* unit;
    
        if(size>GIGA_BYTE)
        {
            floating_size=((double) size)/((double)GIGA_BYTE);
            
            unit="GB";
        }
        else
        {
            if(size>MEGA_BYTE)
            {
                floating_size=((double) size)/((double)MEGA_BYTE);
                
                unit="MB";
            }
            else
            {
                if(size>KILO_BYTE)
                {
                    floating_size=((double) size)/((double)KILO_BYTE);
                    
                    unit="KB";
                }
                else
                {
                    floating_size=((double) size)/((double)BYTE);
                
                    unit="B";
                }
                
            }
        }
    
    
    
    sprintf(buf,"%ld bytes ( %.2lf%s )",size,floating_size,unit);
    
    
    
    return buf;
}




void print_hex_buffer(void* buffer,size_t size)
{
   const unsigned char* buf=(const unsigned char*)buffer;

   int ptr=0;
   printf("\n------------------------------------------------\n");
   printf("dump buffer at %lx, size %libytes:",buffer,size);

   for(ptr=0;ptr<size;ptr++)
   {
      if(ptr%16==0)
      {
        printf("\n%08x :",ptr);
      }

      printf(" %02x",buf[ptr]);

   }
   printf("\n------------------------------------------------\n");
   fflush(stdout);
}



static void set_default_hooks()
{
    //__malloc_hook = def_malloc_hook;
    //__free_hook = def_free_hook;
    //__realloc_hook = def_realloc_hook;
   // __memalign_hook = def_memalign_hook;
}


static void set_instrumenting_hooks()
{
    __malloc_hook = mp_malloc_hook;
    __free_hook = mp_free_hook;
    __realloc_hook = mp_realloc_hook;
    __memalign_hook = mp_memalign_hook;
}

void
libstat_Start ( void )
{
    //printf("Installing Malloc Instrumentation Hooks\n");
    //def_malloc_hook = __malloc_hook;
    ////def_free_hook = __free_hook;
    //def_realloc_hook = __realloc_hook;
    //def_memalign_hook = __memalign_hook;

    mem_allocated=0;
    mem_alloc_peak=0;
    mem_alloc_peak2=0;
    mem_alloc_cnt=0;
    mem_free_cnt=0;
    mem_realloc_cnt=0;
    mem_memalign_cnt=0;

    set_instrumenting_hooks();
}

void
libstat_Stop ( void )
{
    /* Restore all old hooks */
    set_default_hooks();
    
    mem_allocated=0;
    mem_alloc_peak=0;
    mem_alloc_peak2=0;
    mem_alloc_cnt=0;
    mem_free_cnt=0;
    mem_realloc_cnt=0;
    mem_memalign_cnt=0;
}

static void *
mp_memalign_hook (size_t alignement, size_t size, const void *caller )
{
	void *result;
	
	set_default_hooks();
	
	
	/* Call recursively */
	result = memalign ( alignement,size );
	
	mem_memalign_cnt++;
	
	mem_allocated+=malloc_usable_size(result);
	
	if(mem_allocated>mem_alloc_peak)
	{
	   mem_alloc_peak=mem_allocated;
	}
	if(mem_allocated>mem_alloc_peak2)
	{
	   mem_alloc_peak2=mem_allocated;
	}
	
	set_instrumenting_hooks();
	return result;
}

static void *
mp_realloc_hook ( void* ptr,size_t size, const void *caller )
{
	void *result;
	set_default_hooks();
	
	size_t old_mem_size=malloc_usable_size(ptr);
	mem_allocated-=old_mem_size;
	
	/* Call recursively */
	result = realloc ( ptr,size );

	mem_realloc_cnt++;
	
	mem_allocated+=malloc_usable_size(result);
	
	if(mem_allocated>mem_alloc_peak)
	{
	   mem_alloc_peak=mem_allocated;
	}
	if(mem_allocated>mem_alloc_peak2)
	{
	   mem_alloc_peak2=mem_allocated;
	}
	
	set_instrumenting_hooks();
	return result;
}


static void *
mp_malloc_hook ( size_t size, const void *caller )
{
	void *result;
	set_default_hooks();
	
	/* Call recursively */
	result = malloc ( size );

        

	mem_alloc_cnt++;
	
	mem_allocated+=malloc_usable_size(result);
	
	if(mem_allocated>mem_alloc_peak)
	{
	   mem_alloc_peak=mem_allocated;
	}
	if(mem_allocated>mem_alloc_peak2)
	{
	   mem_alloc_peak2=mem_allocated;
	}
	
	set_instrumenting_hooks();
	return result;
}

static void
mp_free_hook ( void *ptr, const void *caller )
{
	/* Restore all old hooks */
	set_default_hooks();
	
	mem_free_cnt++;
	
	mem_allocated-=malloc_usable_size(ptr);
	
	/* Call recursively */
	free ( ptr );
	
	/* Save underlying hooks */
	set_instrumenting_hooks();
}



void libstat_ResetMemStats()
{
    mem_allocated=0;
    mem_alloc_peak=0;
    mem_alloc_cnt=0;
    mem_free_cnt=0;
    mem_realloc_cnt=0;
    mem_memalign_cnt=0;

}
  
void libstat_GetMemStats(int64_t *pmem_allocated,
                         int64_t *pmem_alloc_peak,
                         int64_t *pmem_alloc_peak2,
                         int64_t *pmem_alloc_cnt,
                         int64_t *pmem_free_cnt,
                         int64_t *pmem_realloc_cnt,
                         int64_t *pmem_memalign_cnt)
{
    *pmem_allocated=mem_allocated;
    *pmem_alloc_peak=mem_alloc_peak;
    *pmem_alloc_peak2=mem_alloc_peak2;
    *pmem_alloc_cnt=mem_alloc_cnt;
    *pmem_free_cnt=mem_free_cnt;
    *pmem_realloc_cnt=mem_realloc_cnt;
    *pmem_memalign_cnt=mem_memalign_cnt;
    
    //reset peak2
    mem_alloc_peak2=mem_allocated;
}

size_t libstat_GetMemoryAllocPeak()
{
	return mem_alloc_peak;
}

size_t libstat_GetMemoryAllocated()
{
  return mem_allocated;
}

void libstat_ResetPeak()
{
  mem_alloc_peak=0;
}


void libstat_ShowMemStats()
{
    char buf[256];
    
    fflush(stdout);
    
    /*printf("\n------------------------------------------------------\n");
    
    printf("Memory currently allocated via malloc: %s (if end of program then it should be 0)\n",
        format_human_readable_size(buf,mem_allocated));
    printf("Memory allocation peak via malloc: %s \n",
        format_human_readable_size(buf,mem_alloc_peak));
        
    printf("Total call count: malloc %li free %li memalign %li realloc %li\n",
        mem_alloc_cnt,mem_free_cnt,mem_memalign_cnt,mem_realloc_cnt);*/
    
    
    

    fprintf(stderr,"\n---------------  LIB STAT   --------------------------\n");
    
    fprintf(stderr,"->Memory currently allocated via malloc: %s\n",
        format_human_readable_size(buf,mem_allocated));
    fprintf(stderr,"->Memory allocation peak via malloc: %s \n",
        format_human_readable_size(buf,mem_alloc_peak));
        
    fprintf(stderr,"->Total call count: malloc %li free %li memalign %li realloc %li\n",
        mem_alloc_cnt,mem_free_cnt,mem_memalign_cnt,mem_realloc_cnt);
    fprintf(stderr,"\n------------------------------------------------------\n");
    fflush(stderr);
    
}



/*To Fortran Interface:

    libstat_ShowMemStats()
    libstat_Start()
    libstat_Stop()
    libstat_GetMemStats(int64_t *pmem_allocated,
                         int64_t *pmem_alloc_peak,
                         int64_t *pmem_alloc_cnt,
                         int64_t *pmem_free_cnt,
                         int64_t *pmem_realloc_cnt,
                         int64_t *pmem_memalign_cnt)
    libstat_ResetMemStats()

*/
/*
    these wrappers will be modified by f2cintf.py
*/
/*
void FORT_BIND(libstat_ShowMemStats_f)()
{
    libstat_ShowMemStats();
}


void FORT_BIND  (libstat_Start_f)()
{
    libstat_Start();
}


void FORT_BIND(libstat_Stop_f)()
{
    libstat_Stop();
}


void FORT_BIND(libstat_ResetMemStats_f)()
{
    libstat_ResetMemStats();
}


void FORT_BIND(libstat_GetMemStats_f) (int64_t *pmem_allocated,
                         int64_t *pmem_alloc_peak,
                         int64_t *pmem_alloc_peak2,
                         int64_t *pmem_alloc_cnt,
                         int64_t *pmem_free_cnt,
                         int64_t *pmem_realloc_cnt,
                         int64_t *pmem_memalign_cnt)
{
    libstat_GetMemStats(pmem_allocated,
                         pmem_alloc_peak,
                         pmem_alloc_peak2,
                         pmem_alloc_cnt,
                         pmem_free_cnt,
                         pmem_realloc_cnt,
                         pmem_memalign_cnt);
}


void FORT_BIND(libstat_ResetPeak_f)()
{
    libstat_ResetPeak();
}*/
